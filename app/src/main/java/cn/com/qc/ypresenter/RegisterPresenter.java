package cn.com.qc.ypresenter;

import android.util.Log;

import java.util.List;

import cn.com.qc.bean.CityBean;
import cn.com.qc.bean.EditIntroBean;
import cn.com.qc.help.ServerApi;
import cn.com.qc.main.RegisterActivity;
import cn.com.qc.ymodel.RegisterModel;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by lenovo on 2017/12/20.
 */

public class RegisterPresenter extends BasePresenter<RegisterActivity,RegisterModel>{
    @Override
    public RegisterModel getModel() {
        return new RegisterModel();
    }

    public void sendCode(final String url,final String phone,final int dataType){//1
        Subscription subscription = ServerApi.subscribe(new Observable.OnSubscribe<EditIntroBean>() {
            @Override
            public void call(Subscriber<? super EditIntroBean> subscriber) {
                m.sendCode(subscriber,url,phone,dataType);
            }
        }, new Observer<EditIntroBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                v.onSendCodeError();
            }

            @Override
            public void onNext(EditIntroBean e) {
                v.onSendCodeSuccess(e.getInfoCode(),e.getMessage());
            }
        });
        list.add(subscription);
    }

    public void register(final String url,final String phone,final String password,final String code){
        Subscription subscription = ServerApi.subscribe(new Observable.OnSubscribe<EditIntroBean>() {
            @Override
            public void call(Subscriber<? super EditIntroBean> subscriber) {
                m.register(subscriber,url,phone,password,code);
                Log.i("TAG","url:" + url + ",phone:" + phone + ",password:" + password + ",code:" + code);
            }
        }, new Observer<EditIntroBean>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                v.onRegisterError();
            }

            @Override
            public void onNext(EditIntroBean e) {
                v.onRegisterSuccess(e.getInfoCode(),e.getMessage());
            }
        });
        list.add(subscription);
    }

}
