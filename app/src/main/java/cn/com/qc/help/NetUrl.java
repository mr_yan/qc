package cn.com.qc.help;

/**
 * Created by lenovo on 2017/9/30.
 */

public class NetUrl {

    //获取图片头部
    public static final String ImgHeader = "http://39.106.8.72:8080/qicheng/mobile/getBasePath.do";

    public static final String DNS = "http://39.106.8.72:8080/qicheng/";
    //<-- 首页URL -->
    //首页数据
    public  static final String HomeData = "home/getHomeData.do";
    //首页搜索框
    public static final String HomeSearch = "home/homeTopSearch.do";
    //首页中部搜索
    public static final String HomeCondition = "home/homeBodySearch.do";

    public static final String FindHotInfo = "home/getHomeIndustry.do";

    public static final String GetInfo = "student/getStudentResume.do";

    //完善个人简历
    public static final String UpdateInfo = "student/updateResume.do";
    //图片上传
    public static final String upImage = "img/fildUpload.do";
    //多张图片上传
    public static final String upImageMore = "img/fildUploads.do";

    //公司详情
    public static final String CompanyDetails = "mobile/getEnterpriseById.do";

    //发送验证码
    public static final String SendCode = "student/sendMassage.do";
    //注册
    public static final String Register = "student/userReg.do";
    //忘记密码
    public static final String Forget = "student/forgetPwd.do";
    //登录
    public static final String Login = "student/login.do";
    //退出
    public static final String Logout = "student/logOut.do";

    //身份证上传
    public static final String CardUp = "student/saveCardId.do";

    //发现
    public static final String Find = "mobile/findHot.do";

    //获取所有城市
    public static final String AllCity = "http://restapi.amap.com/v3/config/district";
}
